#pragma once
#include "IApp.h"


class BasicMeshApp_Impl;


//******************************************************************************
//******************************************************************************
class BasicMeshApp : public IApp
{
public:
	BasicMeshApp();
	virtual ~BasicMeshApp();

	virtual void Startup();
	virtual void Enable();
	virtual bool ProcessEvent( const SDL_Event& evt );
	virtual void Update();
	virtual void Disable();
	virtual void Shutdown();

private:
	BasicMeshApp_Impl* m_ModeImpl;
};
