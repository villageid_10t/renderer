#include "stdafx.h"

#include "Logger.h"


//******************************************************************************
//******************************************************************************
//------------------------------------------------------------------------------
void Logger::Debug( const char* fmt, ... )
{
	va_list args;
	va_start( args, fmt );
	SDL_LogMessageV( SDL_LOG_CATEGORY_APPLICATION, SDL_LogPriority::SDL_LOG_PRIORITY_DEBUG, fmt, args ); 
	va_end( args );
}


//------------------------------------------------------------------------------
void Logger::Info( const char* fmt, ... )
{
	va_list args;
	va_start( args, fmt );
	SDL_LogMessageV( SDL_LOG_CATEGORY_APPLICATION, SDL_LogPriority::SDL_LOG_PRIORITY_INFO, fmt, args ); 
	va_end( args );
}


//------------------------------------------------------------------------------
void Logger::Warn( const char* fmt, ... )
{
	va_list args;
	va_start( args, fmt );
	SDL_LogMessageV( SDL_LOG_CATEGORY_APPLICATION, SDL_LogPriority::SDL_LOG_PRIORITY_WARN, fmt, args ); 
	va_end( args );
}


//------------------------------------------------------------------------------
void Logger::Error( const char* fmt, ... )
{
	va_list args;
	va_start( args, fmt );
	SDL_LogMessageV( SDL_LOG_CATEGORY_APPLICATION, SDL_LogPriority::SDL_LOG_PRIORITY_ERROR, fmt, args ); 
	va_end( args );
}


//------------------------------------------------------------------------------
void Logger::Fatal( int exitCode, const char* fmt, ... )
{
	va_list args;
	va_start( args, fmt );
	SDL_LogMessageV( SDL_LOG_CATEGORY_APPLICATION, SDL_LogPriority::SDL_LOG_PRIORITY_CRITICAL, fmt, args ); 
	va_end( args );

	exit(2);
}
