#include "stdafx.h"
#include "shaders.h"
#include "fileio.h"


//------------------------------------------------------------------------------
void attach_shader( GLuint program, GLenum type, const char * code )
{
	GLuint shader = glCreateShader( type );
	glShaderSource( shader, 1, &code, NULL );
	glCompileShader( shader );
	glAttachShader( program, shader );
	glDeleteShader( shader );
}


//------------------------------------------------------------------------------
GLuint build_program( const char* vertSource, const char* fragSource, const char* programName )
{
	GLuint program = glCreateProgram();
	glObjectLabel( GL_PROGRAM, program, -1, programName );

	attach_shader( program, GL_VERTEX_SHADER, vertSource );
	attach_shader( program, GL_FRAGMENT_SHADER, fragSource );	

	glLinkProgram( program );

	GLint result;
	glGetProgramiv( program, GL_LINK_STATUS, &result );
	if( result != GL_TRUE )
	{
		char msg[ 10240 ];
		glGetProgramInfoLog( program, 10240, NULL, msg );
		fprintf( stderr, "Linking program failed: [%s]\n%s\n", programName, msg );
		abort();
	}
	
	char name[255];
	GLsizei len = 0;
	glGetObjectLabel( GL_PROGRAM, program, sizeof(name), &len, name );
	printf( "Built Program: [%s]\n", name );

	return program;
}


//------------------------------------------------------------------------------
GLuint LoadProgram( const char* programName )
{
	std::string assetBase("../Assets/");
	std::string vertFilename = assetBase + std::string(programName) + std::string(".vert");
	std::string vertSource;
	size_t nRead = ReadTextFile( vertFilename.c_str(), vertSource );
	if( nRead <= 0 )
	{
		fprintf( stderr, "Failed to read %s\n", vertFilename );
		return -1;
	}


	std::string fragFilename = assetBase + std::string(programName) + std::string(".frag");
	std::string fragSource;
	nRead = ReadTextFile( fragFilename.c_str(), fragSource );
	if( nRead <= 0 )
	{
		fprintf( stderr, "Failed to read %s\n", fragFilename );
		return -1;
	}	

	GLuint program = build_program( vertSource.c_str(), fragSource.c_str(), programName );

	return program;
}
