#include "stdafx.h"
#include "mesh.h"

#include <rapidjson\document.h>

using namespace rapidjson;


//------------------------------------------------------------------------------
bool LoadMeshFromText( const char* text, Mesh& mesh )
{
	Document d;
	d.Parse( text );


	return false;
}


//------------------------------------------------------------------------------
void BuildQuadMesh( Mesh& mesh )
{
	ZeroMemory( &mesh, sizeof(Mesh) );

	mesh.m_PrimitiveType = GL_TRIANGLES;
	mesh.m_NumVerts = 6;
	
	// position channel
	//int iChannel = 0;
	//mesh.m_VertChannels[iChannel].m_ChannelName = 'POSN';
	//mesh.m_VertChannels[iChannel].m_ChannelOffset = 0;
	//mesh.m_VertChannels[iChannel].m_ChannelStride = 3;
	//mesh.m_VertChannels[iChannel].m_ChannelSize = 4;
	//mesh.m_VertChannels[iChannel].m_ChannelType = GL_FLOAT;
	
	// normal channel
	//++iChannel;
	//mesh.m_VertChannels[iChannel].m_ChannelName = 'NORM';
	//mesh.m_VertChannels[iChannel].m_ChannelOffset = 0;
	//mesh.m_VertChannels[iChannel].m_ChannelStride = 4;
	//mesh.m_VertChannels[iChannel].m_ChannelSize = 4;
	//mesh.m_VertChannels[iChannel].m_ChannelType = GL_FLOAT;

	// color channel
	//++iChannel;
	//mesh.m_VertChannels[iChannel].m_ChannelName = 'COLR';
	//mesh.m_VertChannels[iChannel].m_ChannelOffset = 0;
	//mesh.m_VertChannels[iChannel].m_ChannelStride = 4;
	//mesh.m_VertChannels[iChannel].m_ChannelSize = 4;
	//mesh.m_VertChannels[iChannel].m_ChannelType = GL_FLOAT;

	float size = 0.5f;
	glm::vec3 pos[] =
	{
		glm::vec3( -size, -size, 0.0f ), // 0
		glm::vec3(  size, -size, 0.0f ), // 1
		glm::vec3( -size,  size, 0.0f ), // 2

		glm::vec3(  size,  size, 0.0f ), // 3
		glm::vec3( -size,  size, 0.0f ), // 2
		glm::vec3(  size, -size, 0.0f ), // 1
	};

	//glm::vec3 norm[] =
	//{
	//	glm::vec3( 0.0f, 0.0f, 1.0f ),
	//	glm::vec3( 0.0f, 0.0f, 1.0f ),
	//	glm::vec3( 0.0f, 0.0f, 1.0f ),
	//						   
	//	glm::vec3( 0.0f, 0.0f, 1.0f ),
	//	glm::vec3( 0.0f, 0.0f, 1.0f ),
	//	glm::vec3( 0.0f, 0.0f, 1.0f ),
	//};

	//glm::vec3 color[] =
	//{
	//	glm::vec3( 0.0f, 0.5f, 1.0f ),
	//	glm::vec3( 0.0f, 0.5f, 1.0f ),
	//	glm::vec3( 0.0f, 0.5f, 1.0f ),

	//	glm::vec3( 0.0f, 0.5f, 1.0f ),
	//	glm::vec3( 0.0f, 0.5f, 1.0f ),
	//	glm::vec3( 0.0f, 0.5f, 1.0f ),
	//};

	uint32_t index[] =
	{
		0, 1, 2,
		3, 2, 1,
	};
	
	uint32_t nVertBytes = sizeof(pos);// + sizeof(norm) + sizeof(color);
	uint32_t nIndexBytes = sizeof(index);

	mesh.m_VertData = new uint8_t[ nVertBytes ];
	mesh.m_VertDataSize = nVertBytes;

	mesh.m_IndexData = new uint8_t[ nIndexBytes ];
	mesh.m_IndexDataSize = nIndexBytes;

	uint32_t iData = 0;
	memcpy( &mesh.m_VertData[iData], pos, sizeof(pos) );
	iData += sizeof(pos);
	//memcpy( &mesh.m_VertData[iData], norm, sizeof(norm) );
	//iData += sizeof(norm);
	//memcpy( &mesh.m_VertData[iData], color, sizeof(color) );
	//iData += sizeof(norm);

	memcpy( &mesh.m_IndexData[0], index, sizeof(index) );
}
