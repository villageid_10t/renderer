#include "stdafx.h"

#include "fileio.h"


//------------------------------------------------------------------------------
size_t ReadTextFile( const char* filename, std::string& contents )
{
	FILE* pFile = fopen( filename , "r" );
	if( pFile == nullptr )
	{
		return 0;
	}
	
	fseek( pFile , 0 , SEEK_END );
	long fileSize = ftell( pFile );
	rewind( pFile );

	contents.resize( fileSize );
		
	size_t nRead = fread( &contents[0], 1, fileSize, pFile );

	fclose( pFile );

	return nRead;
}
