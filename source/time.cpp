#include "stdafx.h"
#include "Time.h"


//******************************************************************************
//******************************************************************************
Uint64 Time::hiresStartTicks = 0;
Uint64 Time::hiresNowTicks = 0;
Uint64 Time::hiresThenTicks = 0;	
Uint64 Time::hiresDeltaTicks = 0;
Uint64 Time::hiresFrequency = 0;	


//------------------------------------------------------------------------------
void Time::Init()
{
	Time::hiresStartTicks = SDL_GetPerformanceCounter();
	Time::hiresNowTicks = 0;
	Time::hiresThenTicks = 0;
	Time::hiresDeltaTicks = 0;
	Time::hiresFrequency =  SDL_GetPerformanceFrequency();
}


//------------------------------------------------------------------------------
void Time::Tick()
{
	Time::hiresThenTicks = hiresNowTicks;
	Time::hiresNowTicks= SDL_GetPerformanceCounter() - Time::hiresStartTicks;
	Time::hiresFrequency = SDL_GetPerformanceFrequency();
	Time::hiresDeltaTicks = hiresNowTicks - hiresThenTicks;
}


//------------------------------------------------------------------------------
double Time::Now()
{
	return (double)Time::hiresNowTicks / (double)Time::hiresFrequency;
}


//------------------------------------------------------------------------------
double Time::Delta()
{
	return (double)Time::hiresDeltaTicks / (double)Time::hiresFrequency;
}


//------------------------------------------------------------------------------
void Time::Reset()
{
	Time::Init();
}    
