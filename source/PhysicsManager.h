#pragma once


//******************************************************************************
//******************************************************************************
class PhysicsManager : public IEngineManager
{
public:
	static const char* Type(); 

	PhysicsManager();
	virtual ~PhysicsManager();
	
	const char* GetType();
};
