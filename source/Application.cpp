#include "stdafx.h"
#include <queue>
#include <map>

#include "Application.h"
#include "CommandArgs.h"
#include "Logger.h"
#include "Time.h"

#include "WindowManager.h"
#include "GraphicsManager.h"
#include "PhysicsManager.h"
#include "AudioManager.h"

#include "BasicMeshApp.h"


//******************************************************************************
//******************************************************************************
class Application_Impl
{
private:
	std::queue<IApp*> m_AppModes;
	IApp* m_CurrentAppMode;
	Uint32 m_CurrentAppIndex;

	std::map<const char*,IEngineManager*> m_ManagerCollection;

	bool s_bRunning = true;


public:
	//--------------------------------------------------------------------------
	void Startup( int argc, char *argv[] )
	{
		CommandArgs::Parse( argc, argv );

		Time::Init();		

		int res = SDL_Init( SDL_INIT_VIDEO );
		if( res < 0 )
		{
			Logger::Fatal( -1, "Couldn't initialize SDL\n%s", SDL_GetError() );
		}

		atexit( SDL_Quit );

		WindowManager* windowManager = new WindowManager();
		Application::AddManager( windowManager );

		GraphicsManager* graphicsManager = new GraphicsManager();
		Application::AddManager( graphicsManager );

		PhysicsManager* physicsManager = new PhysicsManager();
		Application::AddManager( physicsManager );

		m_AppModes.push( new BasicMeshApp() );
	}


	//--------------------------------------------------------------------------
	bool Update()
	{
		Time::Tick();

		double now   = Time::Now();
		double delta = Time::Delta();
		//Logger::Info( "%f    %f", now, delta );

		if( m_CurrentAppMode == nullptr )
		{
			m_CurrentAppMode = m_AppModes.front();
			m_AppModes.pop();

			if( m_CurrentAppMode != nullptr )
			{
				m_CurrentAppMode->Startup();
				m_CurrentAppMode->Enable();
			}
		}
		
		if( m_CurrentAppMode != nullptr )
		{			 
			m_CurrentAppMode->Update();
		}
		
		ProcessEvents();

		return s_bRunning;
	}


	//--------------------------------------------------------------------------
	bool ProcessEvents()
	{
		SDL_Event event;
		while( SDL_PollEvent( &event ) )
		{
			//if( !Application::s_CurrentAppMode->ProcessEvent( event ) )
			{
				ProcessEvent( event );
			}
		}

		return true;
	}
	

	//--------------------------------------------------------------------------
	void ProcessEvent( const SDL_Event& event )
	{
		switch( event.type )
		{
		case SDL_KEYDOWN:
			if( event.key.keysym.sym == SDLK_ESCAPE )
			{
				SDL_Event e;
				e.type = SDL_QUIT;
				SDL_PushEvent( &e );
			}
			break;

		case SDL_QUIT:
			s_bRunning = false;
			break;
		}
	}
	

	//--------------------------------------------------------------------------
	void Shutdown()
	{
	}


	//--------------------------------------------------------------------------
	void AddManager( IEngineManager* manager )
	{
		const char* type = manager->GetType();

		m_ManagerCollection.insert_or_assign( type, manager );
	}


	//--------------------------------------------------------------------------
	IEngineManager* GetManager( const char* type )
	{
		auto iFound = m_ManagerCollection.find( type );
		if( iFound != m_ManagerCollection.end() )
		{
			return iFound->second;
		}

		return nullptr;
	}


private:
	
};



//******************************************************************************
//******************************************************************************
Application_Impl* Application::s_Application( nullptr );


//------------------------------------------------------------------------------
void Application::Startup( int argc, char *argv[] )
{
	s_Application = new Application_Impl();

	s_Application->Startup( argc, argv );
}


//------------------------------------------------------------------------------
bool Application::Update()
{
	return s_Application->Update();
}


//------------------------------------------------------------------------------
void Application::Shutdown()
{
	s_Application->Shutdown();

	delete s_Application;
	s_Application = nullptr;
}


//------------------------------------------------------------------------------
void Application::AddManager( IEngineManager* manager )
{
	s_Application->AddManager( manager );
}


//------------------------------------------------------------------------------
IEngineManager* Application::GetManager( const char* type )
{
	return s_Application->GetManager( type );
}
