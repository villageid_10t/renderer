#pragma once


//******************************************************************************
//******************************************************************************
class IEngineManager
{
public:
	virtual ~IEngineManager() = 0 {};
	virtual const char* GetType() = 0;
};
