#pragma once


//******************************************************************************
//******************************************************************************
class GraphicsManager : public IEngineManager
{
public:
	static const char* Type(); 

	GraphicsManager();
	virtual ~GraphicsManager();
	
	const char* GetType();
};
