#include "stdafx.h"

#include "Application.h"
#include "BasicMeshApp.h"
#include "WindowManager.h"
#include "Time.h"

#include "glcommon.h"
#include "shaders.h"


struct AttribFormatDef
{
	enum struct AttributeType : uint32_t
	{
		None = 0,
		Position,
		Normal,
		UV0,
		Color,
		Tangent,
		Lightmap,
		MAX_ATTRIBUTETYPE
	};


	AttributeType attributeType;
	GLuint bindingidex;
	GLint size;
	GLenum type;
	GLboolean normalize;
	GLuint relativeoffset;	
};


struct BufferDef
{
	enum struct BufferType : uint32_t
	{
		None = 0,
		AttributeBuffer,
		IndexBuffer,
		MAX_BUFFERTYPE
	};

	BufferType bufferType;
	GLsizei size;
	GLuint bindingindex;
	GLintptr offset;
	GLsizei stride;
	void* data;
	GLuint bufferObject;
};


struct MeshDef
{
	GLenum mode;
	GLsizei count;
	GLenum type;
	uint32_t nBufferDefs;
	BufferDef BufferDefs[5];
	uint32_t nAttribFormatDefs;
	AttribFormatDef AttribFormatDefs[8];
	GLuint vertexArrayObject;
};


struct vert
{
	glm::vec3 pos;
	//glm::vec3 norm;
	//glm::vec2 tex;
	//glm::u32  color;
};


//******************************************************************************
//******************************************************************************
class BasicMeshApp_Impl
{
private:
	WindowManager* m_WindowManager;


public:
	GLuint idSurfaceProgram = 0;
	GLuint idxUniformProj = 0;
	GLuint idxUniformView = 0;
	GLuint idxUniformModel = 0;
	GLuint idxUniformColor = 0;
	GLuint idxAttribPosition = 0;

	double hiresElapsedS = 0;


	MeshDef mesh;

	BasicMeshApp_Impl::BasicMeshApp_Impl()
	{}


	void Startup()
	{

		m_WindowManager = (WindowManager*)Application::GetManager( WindowManager::Type() );

		if (m_WindowManager != nullptr)
		{
			m_WindowManager->CreateGLWindow();
		}



		idSurfaceProgram = LoadProgram("basic_V");
		
		// Grab the attribute and uniform locs from the shader
		
		GL_CHECK(idxUniformProj = glGetUniformLocation(idSurfaceProgram, "uni_Projection"));
		printf("uni_Projection = %d\n", idxUniformProj);
			
		GL_CHECK(idxUniformView = glGetUniformLocation(idSurfaceProgram, "uni_View"));
		printf("uni_View = %d\n", idxUniformView);
			
		GL_CHECK(idxUniformModel = glGetUniformLocation(idSurfaceProgram, "uni_Model"));
		printf("uni_Model = %d\n", idxUniformModel);
				
		GL_CHECK(idxAttribPosition = glGetAttribLocation(idSurfaceProgram, "attr_Position"));
		printf("attr_Position = %d\n", idxAttribPosition);

		//GL_CHECK(glGetUniformLocation(idSurfaceProgram, "attr_Color"));
		//printf("vert_Color = %d\n", idxUniformColor);
			
		
				
		
		float size = 0.5f;
		vert verts[] = 
		{
			{ glm::vec3( -size, -size, 0.0f ) }, // 0
			{ glm::vec3(  size, -size, 0.0f ) }, // 1
			{ glm::vec3( -size,  size, 0.0f ) }, // 2

			{ glm::vec3(  size,  size, 0.0f ) }, // 3
			{ glm::vec3( -size,  size, 0.0f ) }, // 2
			{ glm::vec3(  size, -size, 0.0f ) }, // 1
		};


		GLuint index[] =
		{
			0, 1, 2,
			3, 2, 1,
		};




		ZeroMemory(&mesh, sizeof(mesh));
		mesh.mode = GL_TRIANGLES;
		mesh.count = 6;
		mesh.type = GL_UNSIGNED_INT;
		mesh.nAttribFormatDefs = 1;
		mesh.AttribFormatDefs[0].attributeType = AttribFormatDef::AttributeType::Position;
		mesh.AttribFormatDefs[0].bindingidex = 0;
		mesh.AttribFormatDefs[0].size = 3;
		mesh.AttribFormatDefs[0].type = GL_FLOAT;
		mesh.AttribFormatDefs[0].normalize = GL_FALSE;
		mesh.AttribFormatDefs[0].relativeoffset = 0;
		mesh.nBufferDefs = 2;
		mesh.BufferDefs[0].bindingindex = 0;
		mesh.BufferDefs[0].offset = 0;
		mesh.BufferDefs[0].stride = sizeof(vert);
		mesh.BufferDefs[0].bufferType = BufferDef::BufferType::AttributeBuffer;
		mesh.BufferDefs[0].size = sizeof(vert) * _countof(verts);
		mesh.BufferDefs[0].data = verts;
		mesh.BufferDefs[1].bindingindex = (GLuint)-1;
		mesh.BufferDefs[1].offset = (GLuint)-1;
		mesh.BufferDefs[1].stride = (GLuint)-1;
		mesh.BufferDefs[1].bufferType = BufferDef::BufferType::IndexBuffer;
		mesh.BufferDefs[1].size = sizeof(GLuint) * _countof(index);
		mesh.BufferDefs[1].data = index;




		GLuint vertDataSize = _countof(verts) * sizeof(vert);
		GLuint indexDataSize = _countof(index) * sizeof(GLuint);

		// create the VAO
		GL_CHECK(glCreateVertexArrays(1, &mesh.vertexArrayObject));

		// create the VBO
		GL_CHECK(glCreateBuffers(1, &mesh.BufferDefs[0].bufferObject));                                                     // create the Vertex Buffer Object
		GL_CHECK(glNamedBufferStorage(mesh.BufferDefs[0].bufferObject, mesh.BufferDefs[0].size, nullptr, GL_DYNAMIC_STORAGE_BIT));  // allocate the memory for the VBO
		
		// attach VBO to VAO at a given slot
		GL_CHECK(glVertexArrayVertexBuffer(mesh.vertexArrayObject, mesh.BufferDefs[0].bindingindex, mesh.BufferDefs[0].bufferObject, mesh.BufferDefs[0].offset, mesh.BufferDefs[0].stride));

		// create the IBO
		GL_CHECK(glCreateBuffers(1, &mesh.BufferDefs[1].bufferObject));
		GL_CHECK(glNamedBufferStorage(mesh.BufferDefs[1].bufferObject, mesh.BufferDefs[1].size, nullptr, GL_DYNAMIC_STORAGE_BIT));
				
		// attach IBO to VAO
		GL_CHECK(glVertexArrayElementBuffer(mesh.vertexArrayObject, mesh.BufferDefs[1].bufferObject));
		
		// enable the attributes and configure the formats and bind them to their appropriate buffer slot
		GL_CHECK(glEnableVertexArrayAttrib(mesh.vertexArrayObject, idxAttribPosition));
		GL_CHECK(glVertexArrayAttribFormat(mesh.vertexArrayObject, idxAttribPosition, mesh.AttribFormatDefs[0].size, mesh.AttribFormatDefs[0].type, mesh.AttribFormatDefs[0].normalize, mesh.AttribFormatDefs[0].relativeoffset));
		GL_CHECK(glVertexArrayAttribBinding(mesh.vertexArrayObject, idxAttribPosition, mesh.AttribFormatDefs[0].bindingidex));
		
		// actually copy the buffer data to the gpu. you can do this every frame for dynamic meshes
		GL_CHECK(glNamedBufferSubData(mesh.BufferDefs[0].bufferObject, 0, mesh.BufferDefs[0].size, mesh.BufferDefs[0].data));
		GL_CHECK(glNamedBufferSubData(mesh.BufferDefs[1].bufferObject, 0, mesh.BufferDefs[1].size, mesh.BufferDefs[1].data));
	}


	void Enable()
	{}


	bool ProcessEvent( const SDL_Event& e )
	{
		return false;
	}


	void Update()
	{
		hiresElapsedS = Time::Now();

		int w, h;
		SDL_GL_GetDrawableSize( m_WindowManager->m_SDLWindow, &w, &h );
		GL_CHECK(glViewport( 0.0f, 0.0f, w, h ));

		GL_CHECK(glClearColor( 0.0f, 0.5f, 1.0f, 0.0f ));
		GL_CHECK(glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT ));

		GL_CHECK(glUseProgram( idSurfaceProgram ));
				
		glm::mat4x4 projection;
		projection = glm::perspectiveFov( 90.0f, (float)w, (float)h, 0.001f, 10.0f );
		GL_CHECK(glUniformMatrix4fv( idxUniformProj, 1, GL_FALSE, glm::value_ptr(projection) ));
				
		glm::mat4x4 view;
		view = glm::translate( glm::vec3( 0.0f, 0.0f, -1.0f ) ) * glm::rotate( (float)hiresElapsedS, glm::vec3(0.0f,1.0f,0.0f) );
		GL_CHECK(glUniformMatrix4fv( idxUniformView, 1, GL_FALSE, glm::value_ptr(view) ));
		
		//glEnable(GL_STENCIL_TEST);
		//glStencilFunc(GL_EQUAL, 1, 0xFF); // Pass test if stencil value is 1
		//glStencilMask(0x00); // Don't write anything to stencil buffer
		//glDepthMask(GL_TRUE); // Write to depth buffer
		
		glm::mat4x4 model;
		//model = glm::translate( model, glm::vec3( 0.25f, 0.25f, 0.0f ) );
		GL_CHECK(glUniformMatrix4fv(idxUniformModel, 1, GL_FALSE, glm::value_ptr(model)));

		//glm::vec4 color(1.0f, 0.0f, 1.0f, 1.0f);
		//GL_CHECK(glUniform4fv(idxUniformColor, 1, glm::value_ptr(color)));

		// draw Interleaved VAO
		GL_CHECK(glBindVertexArray(mesh.vertexArrayObject));
		GL_CHECK(glDrawElements(mesh.mode, mesh.count, mesh.type, nullptr));
		GL_CHECK(glBindVertexArray(0));
				
		//glDepthMask(GL_FALSE);
		//glStencilMask(0xFF);		
		//glDisable(GL_STENCIL_TEST);

		SDL_GL_SwapWindow( m_WindowManager->m_SDLWindow );
	}

	void Disable()
	{}


	void Shutdown()
	{}

};


//******************************************************************************
//******************************************************************************
//------------------------------------------------------------------------------
BasicMeshApp::BasicMeshApp()
{}


//------------------------------------------------------------------------------
BasicMeshApp::~BasicMeshApp()
{}


//------------------------------------------------------------------------------
void BasicMeshApp::Startup()
{
	m_ModeImpl = new BasicMeshApp_Impl();

	m_ModeImpl->Startup();
}


//------------------------------------------------------------------------------
void BasicMeshApp::Enable()
{
	m_ModeImpl->Enable();
}


//------------------------------------------------------------------------------
bool BasicMeshApp::ProcessEvent( const SDL_Event& evt )
{
	return m_ModeImpl->ProcessEvent( evt );
}


//------------------------------------------------------------------------------
void BasicMeshApp::Update()
{
	m_ModeImpl->Update();
}


//------------------------------------------------------------------------------
void BasicMeshApp::Disable()
{
	m_ModeImpl->Disable();
}


//------------------------------------------------------------------------------
void BasicMeshApp::Shutdown()
{
	m_ModeImpl->Shutdown();

	delete m_ModeImpl;
	m_ModeImpl = nullptr;
}
