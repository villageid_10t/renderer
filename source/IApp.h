#pragma once


//******************************************************************************
//******************************************************************************
class IApp
{
public:
	virtual ~IApp() = 0 {};
		
	virtual void Startup() = 0;
	virtual void Enable() = 0;
	virtual bool ProcessEvent( const SDL_Event& e ) = 0;
	virtual void Update() = 0;
	virtual void Disable() = 0;
	virtual void Shutdown() = 0;
		
private:
};
