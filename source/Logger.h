#pragma once


//******************************************************************************
//******************************************************************************
class Logger
{
public:
	static void Debug( const char*, ... );
	static void Info( const char*, ... );
	static void Warn( const char*, ... );
	static void Error( const char*, ... );
	static void Fatal( int exitCode, const char*, ... );
};
