#pragma once


//******************************************************************************
//******************************************************************************
class Time
{
public:
	static void Init();		
	static void Tick();		// Call this once a frame to do frame timing
	static double Now();	// returns the time of the last tick
	static double Delta();	// returns the delta from the previous tick
	static void Reset();	// reset Now to zero

private:
	static Uint64 hiresStartTicks;
	static Uint64 hiresNowTicks;
	static Uint64 hiresThenTicks;	
	static Uint64 hiresDeltaTicks;
	static Uint64 hiresFrequency;	
};
