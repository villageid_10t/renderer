#include "stdafx.h"
#include "Application.h"


//------------------------------------------------------------------------------
int SDL_main( int argc, char *argv[] )
{
	Application::Startup( argc, argv );
	
	while( Application::Update() ) {}

	Application::Shutdown();

	return 0;	
}
