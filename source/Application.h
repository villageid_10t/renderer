#pragma once


class Application_Impl;
class IEngineManager;


//******************************************************************************
//******************************************************************************
class Application
{
public:
	static void Startup( int argc, char *argv[] );
	static bool Update();
	static void Shutdown();

	static void AddManager( IEngineManager* );
	static IEngineManager* GetManager( const char* type );

private:
	static Application_Impl* s_Application;
};
