#include "stdafx.h"
#include <iostream>
#include <sstream>
#include <stdio.h>

#include "ToString.h"
#include "EnumString.h"


#define CASE_STRUCT_TOSTRING( enum, fmt, ... ) case enum: return sprintf_s( buffer, nBytes, #enum##fmt, __VA_ARGS__ )
#define DEFAULT_STRUCT_TOSTRING( enum, fmt, ... ) default: return sprintf_s( buffer, nBytes, #enum##fmt, __VA_ARGS__ )


////******************************************************************************
////******************************************************************************
////------------------------------------------------------------------------------
//int32_t ToString::GL_DEBUG_SOURCE( GLenum source, char* buffer, int32_t nBytes )
//{
//	return sprintf_s( buffer, nBytes, "%s", EnumString::GL_DEBUG_SOURCE(source) );
//}
//
//
////------------------------------------------------------------------------------
//int32_t ToString::GL_DEBUG_TYPE( GLenum type, char* buffer, int32_t nBytes )
//{
//	return sprintf_s( buffer, nBytes, "%s", EnumString::GL_DEBUG_TYPE(type) );
//}
//
//
////------------------------------------------------------------------------------
//int32_t ToString::GL_DEBUG_SEVERITY( GLenum severity, char* buffer, int32_t nBytes )
//{
//	return sprintf_s( buffer, nBytes, "%s", EnumString::GL_DEBUG_SEVERITY(severity) );
//}
//
//
////------------------------------------------------------------------------------
//int32_t ToString::GL_INTERNAL_FORMAT( GLenum internalFormat, char* buffer, int32_t nBytes )
//{
//	return sprintf_s( buffer, nBytes, "%s", EnumString::GL_INTERNAL_FORMAT(internalFormat) );
//}


//------------------------------------------------------------------------------
int32_t ToString::SDL_Event( const ::SDL_Event& event, char* buffer, int nBytes )
{
	switch (event.window.event)
	{
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_NONE,			"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_SHOWN,		"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_HIDDEN,		"%d",		event.window.windowID );
    CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_EXPOSED,		"%d",		event.window.windowID );
    CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_MOVED,		"%d %d %d", event.window.windowID, event.window.data1, event.window.data2 );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_RESIZED,		"%d %d %d", event.window.windowID, event.window.data1, event.window.data2 );
    CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_SIZE_CHANGED,	"%d %d %d", event.window.windowID, event.window.data1, event.window.data2 );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_MINIMIZED,	"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_MAXIMIZED,	"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_RESTORED,		"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_ENTER,		"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_LEAVE,		"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_FOCUS_GAINED,	"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_FOCUS_LOST,	"%d",		event.window.windowID );
	CASE_STRUCT_TOSTRING( SDL_WINDOWEVENT_CLOSE,		"%d",		event.window.windowID );
#if SDL_VERSION_ATLEAST(2, 0, 5)
	CASE_PRINT_STRUCT( SDL_WINDOWEVENT_TAKE_FOCUS,		"%d",		event.window.windowID );
	CASE_PRINT_STRUCT( SDL_WINDOWEVENT_HIT_TEST,		"%d",		event.window.windowID );
#endif
	DEFAULT_STRUCT_TOSTRING( SDL_WINDOWEVENT_INVALID,	"%d",		event.window.windowID );
	}
}
