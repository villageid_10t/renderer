#include "stdafx.h"

#include "WindowManager.h"
#include "Logger.h"
#include "EnumString.h"


static void APIENTRY OnGLDebugMessage( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam );

static const int SCREEN_WIDTH = 512;
static const int SCREEN_HEIGHT = 512;


//******************************************************************************
//******************************************************************************
//------------------------------------------------------------------------------
const char* WindowManager::Type()
{
	static const char* key = "WindowManager";
	return key;
}


//------------------------------------------------------------------------------
WindowManager::WindowManager()
{}


//------------------------------------------------------------------------------
WindowManager::~WindowManager()
{}


//------------------------------------------------------------------------------
const char* WindowManager::GetType()
{
	return Type();
}


//------------------------------------------------------------------------------
bool WindowManager::CreateGLWindow()
{
	// Load the default OpenGL library
	int res = SDL_GL_LoadLibrary( nullptr );
	if( res < 0 )
	{
		Logger::Error( "Failed to load the GL library:\n%s", SDL_GetError() );
		return false;
	}

	// Configure an OpenGL 4.5 context (should be core)
	SDL_GL_SetAttribute( SDL_GL_ACCELERATED_VISUAL, 1 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 5 );
	// Make it a debug context
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG );		
		
	// Configure backbuffer
	SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
	SDL_GL_SetAttribute( SDL_GL_STENCIL_SIZE, 8 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLEBUFFERS , 0 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES , 0 );

	// Create the window
	const char* caption = "OpenGL 4.5";
	m_SDLWindow = SDL_CreateWindow(
		caption,
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE
	);

	if( m_SDLWindow == NULL )
	{
		Logger::Error( "Couldn't set video mode:\n%s", SDL_GetError() );
		return false;
	}

	m_GLContext = SDL_GL_CreateContext( m_SDLWindow );
	if( m_GLContext == NULL )
	{
		Logger::Error( "Failed to create OpenGL context:\n%s", SDL_GetError() );
		return false;
	}
		
	// Check OpenGL properties	
	res = gladLoadGLLoader( SDL_GL_GetProcAddress );
	if( res == 0 )
	{
		Logger::Error ( "Failed to bind OpenGL" );
		return false;
	}

	// Enable GL debugging
	glEnable( GL_DEBUG_OUTPUT );
	glEnable( GL_DEBUG_OUTPUT_SYNCHRONOUS );
	glDebugMessageCallback( OnGLDebugMessage, nullptr );
	glDebugMessageControl( GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, true );
	
	printf( "OpenGL loaded\n" );
	printf( "Vendor:   %s\n", glGetString( GL_VENDOR ) );
	printf( "Renderer: %s\n", glGetString( GL_RENDERER ) );
	printf( "Version:  %s\n", glGetString( GL_VERSION ) );

	SDL_GL_SetSwapInterval( 1 );

	return true;
}


//------------------------------------------------------------------------------
static void APIENTRY OnGLDebugMessage( GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam )
{
	(void)source; (void)type; (void)id;
	(void)severity; (void)length; (void)userParam;
		
	if( source == GL_DEBUG_SOURCE_API )
	{
		return;
	}

	if( severity==GL_DEBUG_SEVERITY_HIGH )
	{
		Logger::Fatal( -1, "[%s] %s | %s (%d)\n%s\n", 
			EnumString::GL_DEBUG_SEVERITY( severity ), 
			EnumString::GL_DEBUG_SOURCE( source ),
			EnumString::GL_DEBUG_TYPE( type ),
			id, 
			message 
		);
	}
	else
	{
		Logger::Debug( "[%s] %s | %s (%d)\n%s\n",
			EnumString::GL_DEBUG_SEVERITY( severity ),
			EnumString::GL_DEBUG_SOURCE( source ),
			EnumString::GL_DEBUG_TYPE( type ),
			id,
			message
		);
	}
}
