#pragma once


//******************************************************************************
//******************************************************************************
class EnumString
{
public:
	static const char*	GL_DEBUG_SOURCE		( GLenum source );
	static const char*	GL_DEBUG_TYPE		( GLenum type );
	static const char*	GL_DEBUG_SEVERITY	( GLenum severity );
	static const char*	GL_INTERNAL_FORMAT	( GLenum internalFormat );
};
 