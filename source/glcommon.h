#pragma once


void CheckOpenGLError(const char* stmt, const char* fname, int line);

#ifdef _DEBUG
#define GL_CHECK(stmt) do { \
            stmt; \
			GLenum err = glGetError(); \
			if (err != GL_NO_ERROR) \
			{ \
				printf("OpenGL error %08x\n"\
				"%s(%i) : %s\n", err, __FILE__, __LINE__, #stmt); \
				abort(); \
			} \
        } while (0)
#else
#define GL_CHECK(stmt) stmt
#endif

