#pragma once

struct SDL_Window;


//******************************************************************************
//******************************************************************************
class WindowManager : public IEngineManager
{
public:
	static const char* Type(); 

	WindowManager();
	virtual ~WindowManager();
	
	const char* GetType();

	bool CreateGLWindow( );
		
	SDL_Window* m_SDLWindow;
	SDL_GLContext m_GLContext;
};
