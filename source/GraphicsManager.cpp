#include "stdafx.h"

#include "GraphicsManager.h"


//******************************************************************************
//******************************************************************************
//------------------------------------------------------------------------------
const char* GraphicsManager::Type()
{
	static const char* key = "GraphicsManager";
	return key;
}


//------------------------------------------------------------------------------
GraphicsManager::GraphicsManager()
{}


//------------------------------------------------------------------------------
GraphicsManager::~GraphicsManager()
{}


//------------------------------------------------------------------------------
const char* GraphicsManager::GetType()
{
	return Type();
}
