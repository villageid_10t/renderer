#include "stdafx.h"


//std::string getTextureParameters(GLuint id)
//{
//    if(glIsTexture(id) == GL_FALSE)
//        return "Not texture object";
//
//    int width, height, format;
//    std::string formatName;
//    glBindTexture(GL_TEXTURE_2D, id);
//    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &width);            // get texture width
//    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &height);          // get texture height
//    glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_INTERNAL_FORMAT, &format); // get texture internal format
//    glBindTexture(GL_TEXTURE_2D, 0);
//
//    formatName = convertInternalFormatToString(format);
//
//    std::stringstream ss;
//    ss << width << "x" << height << ", " << formatName;
//    return ss.str();
//}


//std::string getRenderbufferParameters( GLuint id )
//{
//    if(glIsRenderbuffer(id) == GL_FALSE)
//        return "Not Renderbuffer object";
//
//    int width, height, format, samples;
//    std::string formatName;
//    glBindRenderbuffer(GL_RENDERBUFFER, id);
//    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_WIDTH, &width);       // get renderbuffer width
//    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);     // get renderbuffer height
//    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_INTERNAL_FORMAT, &format); // get renderbuffer internal format
//    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_SAMPLES, &samples);   // get multisample count
//    glBindRenderbuffer(GL_RENDERBUFFER, 0);
//
//    formatName = convertInternalFormatToString(format);
//
//    std::stringstream ss;
//    ss << width << "x" << height << ", " << formatName << ", MSAA(" << samples << ")";
//    return ss.str();
//}


//void printFramebufferInfo( GLuint fbo )
//{
//    // bind fbo
//    glBindFramebuffer(GL_FRAMEBUFFER, fbo);
//
//    std::cout << "\n===== FBO STATUS =====\n";
//
//    // print max # of colorbuffers supported by FBO
//    int colorBufferCount = 0;
//    glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &colorBufferCount);
//    std::cout << "Max Number of Color Buffer Attachment Points: " << colorBufferCount << std::endl;
//
//    // get max # of multi samples
//    int multiSampleCount = 0;
//    glGetIntegerv(GL_MAX_SAMPLES, &multiSampleCount);
//    std::cout << "Max Number of Samples for MSAA: " << multiSampleCount << std::endl;
//
//    int objectType;
//    int objectId;
//
//    // print info of the colorbuffer attachable image
//    for(int i = 0; i < colorBufferCount; ++i)
//    {
//        glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,
//                                              GL_COLOR_ATTACHMENT0+i,
//                                              GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
//                                              &objectType);
//        if(objectType != GL_NONE)
//        {
//            glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,
//                                                  GL_COLOR_ATTACHMENT0+i,
//                                                  GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
//                                                  &objectId);
//
//            std::string formatName;
//
//            std::cout << "Color Attachment " << i << ": ";
//            if(objectType == GL_TEXTURE)
//            {
//                std::cout << "GL_TEXTURE, " << getTextureParameters(objectId) << std::endl;
//            }
//            else if(objectType == GL_RENDERBUFFER)
//            {
//                std::cout << "GL_RENDERBUFFER, " << getRenderbufferParameters(objectId) << std::endl;
//            }
//        }
//    }
//
//    // print info of the depthbuffer attachable image
//    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,
//                                          GL_DEPTH_ATTACHMENT,
//                                          GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
//                                          &objectType);
//    if(objectType != GL_NONE)
//    {
//        glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,
//                                              GL_DEPTH_ATTACHMENT,
//                                              GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
//                                              &objectId);
//
//        std::cout << "Depth Attachment: ";
//        switch(objectType)
//        {
//        case GL_TEXTURE:
//            std::cout << "GL_TEXTURE, " << getTextureParameters(objectId) << std::endl;
//            break;
//        case GL_RENDERBUFFER:
//            std::cout << "GL_RENDERBUFFER, " << getRenderbufferParameters(objectId) << std::endl;
//            break;
//        }
//    }
//
//    // print info of the stencilbuffer attachable image
//    glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,
//                                          GL_STENCIL_ATTACHMENT,
//                                          GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
//                                          &objectType);
//    if(objectType != GL_NONE)
//    {
//        glGetFramebufferAttachmentParameteriv(GL_FRAMEBUFFER,
//                                              GL_STENCIL_ATTACHMENT,
//                                              GL_FRAMEBUFFER_ATTACHMENT_OBJECT_NAME,
//                                              &objectId);
//
//        std::cout << "Stencil Attachment: ";
//        switch(objectType)
//        {
//        case GL_TEXTURE:
//            std::cout << "GL_TEXTURE, " << getTextureParameters(objectId) << std::endl;
//            break;
//        case GL_RENDERBUFFER:
//            std::cout << "GL_RENDERBUFFER, " << getRenderbufferParameters(objectId) << std::endl;
//            break;
//        }
//    }
//
//    std::cout << std::endl;
//    glBindFramebuffer(GL_FRAMEBUFFER, 0);
//}


//bool checkFramebufferStatus(GLuint fbo)
//{
//    // check FBO status
//    glBindFramebuffer(GL_FRAMEBUFFER, fbo); // bind
//    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
//    switch(status)
//    {
//    case GL_FRAMEBUFFER_COMPLETE:
//        std::cout << "Framebuffer complete." << std::endl;
//        return true;
//
//    case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
//        std::cout << "[ERROR] Framebuffer incomplete: Attachment is NOT complete." << std::endl;
//        return false;
//
//    case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
//        std::cout << "[ERROR] Framebuffer incomplete: No image is attached to FBO." << std::endl;
//        return false;
///*
//    case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
//        std::cout << "[ERROR] Framebuffer incomplete: Attached images have different dimensions." << std::endl;
//        return false;
//
//    case GL_FRAMEBUFFER_INCOMPLETE_FORMATS:
//        std::cout << "[ERROR] Framebuffer incomplete: Color attached images have different internal formats." << std::endl;
//        return false;
//*/
//    case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
//        std::cout << "[ERROR] Framebuffer incomplete: Draw buffer." << std::endl;
//        return false;
//
//    case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
//        std::cout << "[ERROR] Framebuffer incomplete: Read buffer." << std::endl;
//        return false;
//
//    case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
//        std::cout << "[ERROR] Framebuffer incomplete: Multisample." << std::endl;
//        return false;
//
//    case GL_FRAMEBUFFER_UNSUPPORTED:
//        std::cout << "[ERROR] Framebuffer incomplete: Unsupported by FBO implementation." << std::endl;
//        return false;
//
//    default:
//        std::cout << "[ERROR] Framebuffer incomplete: Unknown error." << std::endl;
//        return false;
//    }
//    glBindFramebuffer(GL_FRAMEBUFFER, 0);   // unbind
//}
