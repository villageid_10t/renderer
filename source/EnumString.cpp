#include "stdafx.h"

#include "EnumString.h"


#define ENUM_STR_CASE( x ) case x: return "x"
#define ENUM_STR_DEFAULT( x ) default: return "x"


//******************************************************************************
//******************************************************************************
//------------------------------------------------------------------------------
const char* EnumString::GL_DEBUG_SOURCE( GLenum source )
{
	switch( source )
	{
	ENUM_STR_CASE( GL_DEBUG_SOURCE_API );	
	ENUM_STR_CASE( GL_DEBUG_SOURCE_WINDOW_SYSTEM );
	ENUM_STR_CASE( GL_DEBUG_SOURCE_SHADER_COMPILER );
	ENUM_STR_CASE( GL_DEBUG_SOURCE_THIRD_PARTY );
	ENUM_STR_CASE( GL_DEBUG_SOURCE_APPLICATION );	
	ENUM_STR_CASE( GL_DEBUG_SOURCE_OTHER );		
	ENUM_STR_DEFAULT( GL_DEBUG_SOURCE_INVALID );								
	}
}


//------------------------------------------------------------------------------
const char* EnumString::GL_DEBUG_TYPE( GLenum type )
{
	switch( type )
	{
	ENUM_STR_CASE( GL_DEBUG_TYPE_ERROR );
	ENUM_STR_CASE( GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR );
	ENUM_STR_CASE( GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR );
	ENUM_STR_CASE( GL_DEBUG_TYPE_PORTABILITY );
	ENUM_STR_CASE( GL_DEBUG_TYPE_PERFORMANCE );
	ENUM_STR_CASE( GL_DEBUG_TYPE_OTHER );
	ENUM_STR_CASE( GL_DEBUG_TYPE_MARKER );
	ENUM_STR_CASE( GL_DEBUG_TYPE_PUSH_GROUP );
	ENUM_STR_CASE( GL_DEBUG_TYPE_POP_GROUP );
	ENUM_STR_DEFAULT( GL_DEBUG_TYPE_INVALID );
	}
}


//------------------------------------------------------------------------------
const char* EnumString::GL_DEBUG_SEVERITY( GLenum severity )
{
	switch( severity )
	{
	ENUM_STR_CASE( GL_DEBUG_SEVERITY_HIGH );
	ENUM_STR_CASE( GL_DEBUG_SEVERITY_MEDIUM );
	ENUM_STR_CASE( GL_DEBUG_SEVERITY_LOW );
	ENUM_STR_CASE( GL_DEBUG_SEVERITY_NOTIFICATION );
	ENUM_STR_DEFAULT( GL_DEBUG_SEVERITY_INVALID );
	}
}


//------------------------------------------------------------------------------
const char* EnumString::GL_INTERNAL_FORMAT( GLenum internalFormat )
{
    switch(internalFormat)
    {
    ENUM_STR_CASE( GL_STENCIL_INDEX );		// 0x1901
    ENUM_STR_CASE( GL_DEPTH_COMPONENT );		// 0x1902
    ENUM_STR_CASE( GL_ALPHA );				// 0x1906
    ENUM_STR_CASE( GL_RGB );					// 0x1907
    ENUM_STR_CASE( GL_RGBA );				// 0x1908
    //ENUM_STR_CASE( GL_LUMINANCE );			// 0x1909
    //ENUM_STR_CASE( GL_LUMINANCE_ALPHA );		// 0x190A
    ENUM_STR_CASE( GL_R3_G3_B2 );			// 0x2A10
    //ENUM_STR_CASE( GL_ALPHA4 );				// 0x803B
    //ENUM_STR_CASE( GL_ALPHA8 );				// 0x803C
    //ENUM_STR_CASE( GL_ALPHA12 );				// 0x803D
    //ENUM_STR_CASE( GL_ALPHA16 );				// 0x803E
    //ENUM_STR_CASE( GL_LUMINANCE4 );			// 0x803F
    //ENUM_STR_CASE( GL_LUMINANCE8 );			// 0x8040
    //ENUM_STR_CASE( GL_LUMINANCE12 );			// 0x8041
    //ENUM_STR_CASE( GL_LUMINANCE16 );			// 0x8042
    //ENUM_STR_CASE( GL_LUMINANCE4_ALPHA4 );	// 0x8043
    //ENUM_STR_CASE( GL_LUMINANCE6_ALPHA2 );	// 0x8044
    //ENUM_STR_CASE( GL_LUMINANCE8_ALPHA8 );	// 0x8045
    //ENUM_STR_CASE( GL_LUMINANCE12_ALPHA4 );	// 0x8046
    //ENUM_STR_CASE( GL_LUMINANCE12_ALPHA12 );	// 0x8047
    //ENUM_STR_CASE( GL_LUMINANCE16_ALPHA16 );	// 0x8048
    //ENUM_STR_CASE( GL_INTENSITY );			// 0x8049
    //ENUM_STR_CASE( GL_INTENSITY4 );			// 0x804A
    //ENUM_STR_CASE( GL_INTENSITY8 );			// 0x804B
    //ENUM_STR_CASE( GL_INTENSITY12 );			// 0x804C
    //ENUM_STR_CASE( GL_INTENSITY16 );			// 0x804D
    ENUM_STR_CASE( GL_RGB4 );				// 0x804F
    ENUM_STR_CASE( GL_RGB5 );				// 0x8050
    ENUM_STR_CASE( GL_RGB8 );				// 0x8051
    ENUM_STR_CASE( GL_RGB10 );				// 0x8052
    ENUM_STR_CASE( GL_RGB12 );				// 0x8053
    ENUM_STR_CASE( GL_RGB16 );				// 0x8054
    ENUM_STR_CASE( GL_RGBA2 );				// 0x8055
    ENUM_STR_CASE( GL_RGBA4 );				// 0x8056
    ENUM_STR_CASE( GL_RGB5_A1 );				// 0x8057
    ENUM_STR_CASE( GL_RGBA8 );				// 0x8058
    ENUM_STR_CASE( GL_RGB10_A2 );			// 0x8059
    ENUM_STR_CASE( GL_RGBA12 );				// 0x805A
    ENUM_STR_CASE( GL_RGBA16 );				// 0x805B
    ENUM_STR_CASE( GL_DEPTH_COMPONENT16 );	// 0x81A5
    ENUM_STR_CASE( GL_DEPTH_COMPONENT24 );	// 0x81A6
    ENUM_STR_CASE( GL_DEPTH_COMPONENT32 );	// 0x81A7
    ENUM_STR_CASE( GL_DEPTH_STENCIL );		// 0x84F9
    ENUM_STR_CASE( GL_RGBA32F );				// 0x8814
    ENUM_STR_CASE( GL_RGB32F );				// 0x8815
    ENUM_STR_CASE( GL_RGBA16F );				// 0x881A
    ENUM_STR_CASE( GL_RGB16F );				// 0x881B
    ENUM_STR_CASE( GL_DEPTH24_STENCIL8 );	// 0x88F0
    ENUM_STR_DEFAULT( GL_FORMAT_INVALID );
	}
}
