#pragma once 

#include <cstdio>
#include <cstdlib>
#include <stdint.h>
#include <inttypes.h>

#define GLM_FORCE_RADIANS 1
#include <glm\ext.hpp>
#include <SDL.h>
#undef main
#include "glad.h"

#include "IApp.h"
#include "IEngineManager.h"