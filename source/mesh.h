#pragma once


#define NUM_CHANNELS (16)


//******************************************************************************
//******************************************************************************
struct Mesh
{
	enum class ChannelName : uint32_t
	{
		Position,
		Normal,
		Color,
		UV0,
		UV1,
		Lightmap,
		Tangent,
		Binormal,
		AUX_0,
		AUX_MAX = AUX_0 + 8,
	};

	struct ChannelInfo
	{
		ChannelName m_Name;
		uint32_t m_Offset;
		uint32_t m_Stride;
		uint32_t m_Size;
		uint32_t m_Type;
	};

	uint32_t m_PrimitiveType;		
	uint32_t m_NumVerts;
	ChannelInfo m_VertChannels[ NUM_CHANNELS ];
	uint8_t* m_VertData;
	uint32_t m_VertDataSize;
	uint8_t* m_IndexData;
	uint32_t m_IndexDataSize;
};


//------------------------------------------------------------------------------
void BuildQuadMesh( Mesh& mesh );
