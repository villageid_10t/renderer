#version 450 core

uniform	mat4	vert_Proj;
uniform	mat4	vert_View;
uniform	mat4	vert_Model;
//uniform	vec4	vert_Color;

in		vec3	vert_Pos;

//out		vec4	frag_Color;

void main(void)
{
	//frag_Color = vert_Color;
	gl_Position = vert_Proj * vert_View * vert_Model * vec4( vert_Pos, 1.0f );
}

