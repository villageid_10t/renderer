#version 450 core


// in vec4 gl_FragCoord;
// in bool gl_FrontFacing;
// in vec2 gl_PointCoord;
// in int gl_SampleID;
// in vec2 gl_SamplePosition;
// in int gl_SampleMaskIn[];
// in float gl_ClipDistance[];
// in int gl_PrimitiveID;
// in int gl_Layer;
// in int gl_ViewportIndex;

readonly restrict uniform layout(rgba8) image2D	image;

layout(location=0) out vec4 color;

void main(void)
{
	color = imageLoad(image, ivec4(gl_FragCoord).xy);
}