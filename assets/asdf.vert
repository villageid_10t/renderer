#version 450 core

uniform mat4 uni_Model;
uniform mat4 uni_View;
uniform mat4 uni_Proj;

layout(location = 0) in vec3 attr_Position;
//layout(location = 1) in vec3 attr_Color;
//layout(location = 2) in vec2 attr_Texcoord0;

out gl_PerVertex
{
  vec4 gl_Position;
  float gl_PointSize;
  float gl_ClipDistance[];
};

//out vec3 frag_Color;
//out vec2 frag_Texcoord;

void main()
{
    gl_Position = uni_Proj * uni_View * uni_Model * vec4(attr_Position.xyz, 1.0);
};