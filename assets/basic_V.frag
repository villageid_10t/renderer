#version 450 core
// gl default
in vec4 gl_FragCoord;
//in bool gl_FrontFacing;
in vec2 gl_PointCoord;

//in int gl_SampleID;
//in vec2 gl_SamplePosition;
in int gl_SampleMaskIn[];

in float gl_ClipDistance[];
in int gl_PrimitiveID;

in int gl_Layer;
in int gl_ViewportIndex;

// mine 
//in vec3 frag_Color;
//in vec2 frag_Texcoord;


// gl default
out float gl_FragDepth;

// mine 
out vec4 outColor;


void main()
{
    outColor = vec4( 1,0,0,1);
};