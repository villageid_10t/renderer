#version 450 core

// in int gl_VertexID;
// in int gl_InstanceID;
layout(location=0) in vec2 coord;


// out vec4 gl_Position;
// out float gl_PointSize;
// out float gl_ClipDistance[];


void main(void)
{
	gl_Position = vec4(coord, 0.0, 1.0);
}