#version 450 core
in vec4 gl_FragCoord;
in bool gl_FrontFacing;
in vec2 gl_PointCoord;

//in int gl_SampleID;
//in vec2 gl_SamplePosition;
in int gl_SampleMaskIn[];

in float gl_ClipDistance[];
in int gl_PrimitiveID;

in int gl_Layer;
in int gl_ViewportIndex;

out float gl_FragDepth;




in vec3 Color;
in vec2 Texcoord;
out vec4 outColor;
uniform sampler2D texKitten;
uniform sampler2D texPuppy;

void main()
{
    outColor = vec4(Color, 1.0) * mix(texture(texKitten, Texcoord), texture(texPuppy, Texcoord), 0.5);
};